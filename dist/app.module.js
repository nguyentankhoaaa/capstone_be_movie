"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const quan_ly_dat_ve_module_1 = require("./quan-ly-dat-ve/quan-ly-dat-ve.module");
const quay_ly_nguoi_dung_module_1 = require("./quay-ly-nguoi-dung/quay-ly-nguoi-dung.module");
const config_1 = require("@nestjs/config");
const quan_ly_phim_module_1 = require("./quan-ly-phim/quan-ly-phim.module");
const quan_ly_rap_module_1 = require("./quan-ly-rap/quan-ly-rap.module");
let AppModule = exports.AppModule = class AppModule {
};
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [quan_ly_dat_ve_module_1.QuanLyDatVeModule, quay_ly_nguoi_dung_module_1.QuayLyNguoiDungModule,
            config_1.ConfigModule.forRoot({ isGlobal: true }),
            quan_ly_phim_module_1.QuanLyPhimModule,
            quan_ly_rap_module_1.QuanLyRapModule],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map