import { QuanLyPhimService } from './quan-ly-phim.service';
export declare class QuanLyPhimController {
    private readonly quanLyPhimService;
    constructor(quanLyPhimService: QuanLyPhimService);
    layDanhSachBanner(res: any): Promise<void>;
    layDanhSachPhim(res: any): Promise<void>;
    layDanhSachPhimPhanTrang(res: any, page: any, pageSize: any): Promise<void>;
    LayDanhSanhPhimTheoNgay(res: any, body: any, page: any, pageSize: any): Promise<void>;
    themPhimUploadHinh(res: any, body: any, file: any): Promise<void>;
    CapNhatPhimUpload(res: any, file: any, maPhim: any, body: any): Promise<void>;
    XoaPhim(res: any, maPhim: any): Promise<void>;
}
