"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyPhimController = void 0;
const common_1 = require("@nestjs/common");
const quan_ly_phim_service_1 = require("./quan-ly-phim.service");
const passport_1 = require("@nestjs/passport");
const decorators_1 = require("@nestjs/common/decorators");
const platform_express_1 = require("@nestjs/platform-express");
const multer_1 = require("multer");
let QuanLyPhimController = exports.QuanLyPhimController = class QuanLyPhimController {
    constructor(quanLyPhimService) {
        this.quanLyPhimService = quanLyPhimService;
    }
    layDanhSachBanner(res) {
        return this.quanLyPhimService.layDanhSachBanner(res);
    }
    layDanhSachPhim(res) {
        return this.quanLyPhimService.layDanhSachPhim(res);
    }
    layDanhSachPhimPhanTrang(res, page, pageSize) {
        return this.quanLyPhimService.layDanhSachPhimPhanTrang(res, Number(page), Number(pageSize));
    }
    LayDanhSanhPhimTheoNgay(res, body, page, pageSize) {
        return this.quanLyPhimService.layDanhSachPhimTheoNgay(res, body, Number(page), Number(pageSize));
    }
    themPhimUploadHinh(res, body, file) {
        return this.quanLyPhimService.themPhimUploadHinh(res, body, file);
    }
    CapNhatPhimUpload(res, file, maPhim, body) {
        return this.quanLyPhimService.capNhatPhimUpload(res, file, Number(maPhim), body);
    }
    XoaPhim(res, maPhim) {
        return this.quanLyPhimService.xoaPhim(res, Number(maPhim));
    }
};
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachBanner"),
    __param(0, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "layDanhSachBanner", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachPhim"),
    __param(0, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "layDanhSachPhim", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/layDanhSachPhimPhanTrang/:page/:page_size"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("page")),
    __param(2, (0, common_1.Param)("page_size")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "layDanhSachPhimPhanTrang", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachPhimTheoNgay/:page/:page_size"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Param)("page")),
    __param(3, (0, common_1.Param)("page_size")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "LayDanhSanhPhimTheoNgay", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("file", {
        storage: (0, multer_1.diskStorage)({
            destination: process.cwd() + "/public/img",
            filename: (req, file, callback) => {
                let newName = new Date().getTime() + "_" + file.originalname;
                callback(null, newName);
            }
        })
    })),
    (0, common_1.Post)("/ThemPhimUploadHinh"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFile)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "themPhimUploadHinh", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("file", {
        storage: (0, multer_1.diskStorage)({
            destination: process.cwd() + "/public/img",
            filename: (req, file, callback) => {
                let newName = new Date().getTime() + "_" + file.originalname;
                callback(null, newName);
            }
        })
    })),
    (0, common_1.Put)("CapNhatPhimUpload/:ma_phim"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.UploadedFile)()),
    __param(2, (0, common_1.Param)("ma_phim")),
    __param(3, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "CapNhatPhimUpload", null);
__decorate([
    (0, decorators_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Delete)("/XoaPhim/:ma_phim"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("ma_phim")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyPhimController.prototype, "XoaPhim", null);
exports.QuanLyPhimController = QuanLyPhimController = __decorate([
    (0, common_1.Controller)('api/QuanLyPhim'),
    __metadata("design:paramtypes", [quan_ly_phim_service_1.QuanLyPhimService])
], QuanLyPhimController);
//# sourceMappingURL=quan-ly-phim.controller.js.map