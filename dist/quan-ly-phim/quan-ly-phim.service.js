"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyPhimService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const response_1 = require("../config/response");
let QuanLyPhimService = exports.QuanLyPhimService = class QuanLyPhimService {
    constructor() {
        this.prisma = new client_1.PrismaClient();
    }
    async layDanhSachBanner(res) {
        try {
            let data = await this.prisma.banner.findMany();
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layDanhSachPhim(res) {
        try {
            let data = await this.prisma.phim.findMany();
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layDanhSachPhimPhanTrang(res, page, page_size) {
        try {
            let index = (page - 1) * page_size;
            let data = await this.prisma.phim.findMany({ skip: index, take: page_size });
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch (e) {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layDanhSachPhimTheoNgay(res, body, page, page_size) {
        try {
            let index = (page - 1) * page_size;
            let checkDate = await this.prisma.phim.findMany({ where: { ngay_khoi_chieu: { lte: new Date(body.denNgay), gte: new Date(body.tuNgay) } }, skip: index, take: page_size });
            if (checkDate) {
                (0, response_1.successCode)(res, checkDate, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "date of movie not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async themPhimUploadHinh(res, body, file) {
        try {
            let hot = !(['false', '0', '', 'undefined'].indexOf(String(body.hot).toLowerCase().trim()) + 1);
            let dangChieu = !(['false', '0', '', 'undefined'].indexOf(String(body.dangChieu).toLowerCase().trim()) + 1);
            let sapChieu = !(['false', '0', '', 'undefined'].indexOf(String(body.sapChieu).toLowerCase().trim()) + 1);
            let newData = {
                ten_phim: body.tenPhim,
                trailer: body.trailer,
                hinh_anh: file.filename,
                mo_ta: body.moTa,
                ngay_khoi_chieu: new Date(body.ngayKhoiChieu),
                danh_gia: Number(body.danhGia),
                hot: Boolean(hot),
                dang_chieu: Boolean(dangChieu),
                sap_chieu: Boolean(sapChieu)
            };
            let data = await this.prisma.phim.create({ data: newData });
            (0, response_1.successCode)(res, data, "create data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async capNhatPhimUpload(res, file, maPhim, body) {
        try {
            let hot = !(['false', '0', '', 'undefined'].indexOf(String(body.hot).toLowerCase().trim()) + 1);
            let dangChieu = !(['false', '0', '', 'undefined'].indexOf(String(body.dangChieu).toLowerCase().trim()) + 1);
            let sapChieu = !(['false', '0', '', 'undefined'].indexOf(String(body.sapChieu).toLowerCase().trim()) + 1);
            let checkMaPhim = await this.prisma.phim.findFirst({ where: { ma_phim: maPhim } });
            let newData = {
                ten_phim: body.tenPhim,
                trailer: body.trailer,
                hinh_anh: file.filename,
                mo_ta: body.moTa,
                ngay_khoi_chieu: new Date(body.ngayKhoiChieu),
                danh_gia: Number(body.danhGia),
                hot: Boolean(hot),
                dang_chieu: Boolean(dangChieu),
                sap_chieu: Boolean(sapChieu)
            };
            if (checkMaPhim) {
                let data = await this.prisma.phim.update({ data: newData, where: { ma_phim: maPhim } });
                (0, response_1.successCode)(res, data, "update movie success");
            }
            else {
                (0, response_1.failCode)(res, null, "movie not exist");
            }
        }
        catch (e) {
            (0, response_1.errorCode)(res, e.message);
        }
    }
    async xoaPhim(res, maPhim) {
        try {
            let checkMaPhim = await this.prisma.phim.findFirst({ where: { ma_phim: maPhim } });
            let checkIdBanner = await this.prisma.banner.findFirst({ where: { ma_phim: maPhim } });
            let checkIdLichChieu = await this.prisma.lichChieu.findFirst({ where: { ma_phim: maPhim } });
            if (checkMaPhim) {
                if (checkIdBanner == null) {
                    if (checkIdLichChieu == null) {
                        await this.prisma.phim.delete({ where: { ma_phim: maPhim } });
                        (0, response_1.successCode)(res, "", "delete movie success");
                    }
                    else {
                        (0, response_1.failCode)(res, null, "movie had movie showtimes");
                    }
                }
                else {
                    (0, response_1.failCode)(res, null, "movie had on banner ");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "movie not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
};
exports.QuanLyPhimService = QuanLyPhimService = __decorate([
    (0, common_1.Injectable)()
], QuanLyPhimService);
//# sourceMappingURL=quan-ly-phim.service.js.map