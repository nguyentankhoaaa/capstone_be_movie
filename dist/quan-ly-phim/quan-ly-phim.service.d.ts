import { PrismaClient } from '@prisma/client';
export declare class QuanLyPhimService {
    prisma: PrismaClient<import(".prisma/client").Prisma.PrismaClientOptions, never, import("@prisma/client/runtime/library").DefaultArgs>;
    layDanhSachBanner(res: any): Promise<void>;
    layDanhSachPhim(res: any): Promise<void>;
    layDanhSachPhimPhanTrang(res: any, page: any, page_size: any): Promise<void>;
    layDanhSachPhimTheoNgay(res: any, body: any, page: any, page_size: any): Promise<void>;
    themPhimUploadHinh(res: any, body: any, file: any): Promise<void>;
    capNhatPhimUpload(res: any, file: any, maPhim: any, body: any): Promise<void>;
    xoaPhim(res: any, maPhim: any): Promise<void>;
}
