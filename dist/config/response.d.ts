declare let successCode: (res: any, data: any, message: any) => any;
declare let failCode: (res: any, data: any, message: any) => any;
declare let errorCode: (res: any, message: any) => any;
export { successCode, failCode, errorCode };
