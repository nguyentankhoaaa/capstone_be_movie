"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorCode = exports.failCode = exports.successCode = void 0;
let successCode = (res, data, message) => {
    return res.status(200).json({
        message,
        content: data
    });
};
exports.successCode = successCode;
let failCode = (res, data, message) => {
    return res.status(400).json({
        message,
        content: data
    });
};
exports.failCode = failCode;
let errorCode = (res, message) => {
    return res.status(500).json({
        message,
    });
};
exports.errorCode = errorCode;
//# sourceMappingURL=response.js.map