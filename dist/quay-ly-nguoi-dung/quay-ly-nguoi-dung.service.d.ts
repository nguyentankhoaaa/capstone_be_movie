import { Prisma, PrismaClient } from '@prisma/client';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
export declare class QuayLyNguoiDungService {
    private jwtService;
    private configService;
    constructor(jwtService: JwtService, configService: ConfigService);
    prisma: PrismaClient<Prisma.PrismaClientOptions, never, import("@prisma/client/runtime/library").DefaultArgs>;
    dangKy(userSignUp: any, res: any): Promise<void>;
    dangNhap(userLogin: any, res: any): Promise<void>;
    getListND(res: any): Promise<void>;
    getUserPhanTrang(page: any, pageSize: any, res: any): Promise<void>;
    searchNguoiDung(res: any, query: any): Promise<void>;
    searchUserPhanTrang(res: any, page: any, pageSize: any, query: any): Promise<void>;
    thongTinTaiKhoan(res: any): Promise<void>;
    layThongTinNguoiDung(res: any, userId: any): Promise<void>;
    themNguoiDung(res: any, user: any): Promise<void>;
    capNhatThongTinNguoiDung(res: any, userId: any, user: any): Promise<void>;
    capNhatThongTinNguoiDungAdmin(res: any, user: any, userId: any): Promise<void>;
    xoaNguoiDung(res: any, userId: any): Promise<void>;
    getListLoaiND(res: any): Promise<void>;
}
