"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuayLyNguoiDungModule = void 0;
const common_1 = require("@nestjs/common");
const quay_ly_nguoi_dung_service_1 = require("./quay-ly-nguoi-dung.service");
const quay_ly_nguoi_dung_controller_1 = require("./quay-ly-nguoi-dung.controller");
const jwt_1 = require("@nestjs/jwt");
const strategy_jwt_1 = require("../strategy/strategy.jwt");
let QuayLyNguoiDungModule = exports.QuayLyNguoiDungModule = class QuayLyNguoiDungModule {
};
exports.QuayLyNguoiDungModule = QuayLyNguoiDungModule = __decorate([
    (0, common_1.Module)({
        imports: [jwt_1.JwtModule.register({})],
        controllers: [quay_ly_nguoi_dung_controller_1.QuayLyNguoiDungController],
        providers: [quay_ly_nguoi_dung_service_1.QuayLyNguoiDungService, strategy_jwt_1.JwtStrategy]
    })
], QuayLyNguoiDungModule);
//# sourceMappingURL=quay-ly-nguoi-dung.module.js.map