import { QuayLyNguoiDungService } from './quay-ly-nguoi-dung.service';
export declare class QuayLyNguoiDungController {
    private readonly quayLyNguoiDungService;
    constructor(quayLyNguoiDungService: QuayLyNguoiDungService);
    getListLoaiND(res: any): Promise<void>;
    dangKy(body: any, res: any): Promise<void>;
    dangNhap(body: any, res: any): Promise<void>;
    getListND(res: any): Promise<void>;
    getUserPhanTrang(res: any, page: any, pageSize: any): Promise<void>;
    SearchNguoiDung(res: any, query: any): Promise<void>;
    SearchUserPhanTrang(res: any, page: any, pageSize: any, query: any): Promise<void>;
    ThongTinTaiKhoan(res: any): Promise<void>;
    LayThongTinNguoiDung(res: any, userId: any): Promise<void>;
    ThemNguoiDung(res: any, body: any): Promise<void>;
    CapNhatThongTinNguoiDung(res: any, userId: any, body: any): Promise<void>;
    CapNhatThongTinNguoiDungAd(res: any, body: any, userId: any): Promise<void>;
    XoaNGuoiDung(res: any, userId: any): Promise<void>;
}
