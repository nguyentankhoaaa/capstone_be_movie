"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuayLyNguoiDungService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const response_1 = require("../config/response");
const bcryptjs_1 = require("bcryptjs");
const jwt_1 = require("@nestjs/jwt");
const config_1 = require("@nestjs/config");
let QuayLyNguoiDungService = exports.QuayLyNguoiDungService = class QuayLyNguoiDungService {
    constructor(jwtService, configService) {
        this.jwtService = jwtService;
        this.configService = configService;
        this.prisma = new client_1.PrismaClient();
    }
    async dangKy(userSignUp, res) {
        try {
            let checkEmail = await this.prisma.nguoiDung.findFirst({ where: { email: userSignUp.email } });
            let checkSDT = await this.prisma.nguoiDung.findFirst({ where: { so_dt: userSignUp.soDt } });
            if (checkEmail == null) {
                if (checkSDT == null) {
                    let newData = { ho_ten: userSignUp.hoTen,
                        email: userSignUp.email,
                        so_dt: userSignUp.soDt,
                        mat_khau: (0, bcryptjs_1.hashSync)(userSignUp.matKhau, 10),
                        loai_nguoi_dung: "customer"
                    };
                    await this.prisma.nguoiDung.create({ data: newData });
                    (0, response_1.successCode)(res, "", "sign up success");
                }
                else {
                    (0, response_1.failCode)(res, null, "numberPhone existed");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "email existed");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    ;
    async dangNhap(userLogin, res) {
        try {
            let checUser = await this.prisma.nguoiDung.findFirst({ where: { email: userLogin.email } });
            if (checUser) {
                if ((0, bcryptjs_1.compareSync)(userLogin.matKhau, checUser.mat_khau)) {
                    let token = this.jwtService.sign({ data: checUser }, { secret: this.configService.get("KEY"), expiresIn: "10m" });
                    (0, response_1.successCode)(res, token, "login success");
                }
                else {
                    (0, response_1.failCode)(res, null, "password wrong");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "email not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async getListND(res) {
        try {
            let data = await this.prisma.nguoiDung.findMany();
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error Be");
        }
    }
    async getUserPhanTrang(page, pageSize, res) {
        try {
            let index = (page - 1) * pageSize;
            let data = await this.prisma.nguoiDung.findMany({
                skip: index,
                take: Number(pageSize)
            });
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async searchNguoiDung(res, query) {
        try {
            let checkName = await this.prisma.nguoiDung.findMany({ where: { ho_ten: { contains: query } } });
            let checkEmail = await this.prisma.nguoiDung.findMany({ where: { email: { contains: query } } });
            if (checkEmail.length !== 0) {
                checkEmail;
                (0, response_1.successCode)(res, checkEmail, "get data success");
            }
            else if (checkName.length !== 0) {
                checkName;
                (0, response_1.successCode)(res, checkName, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async searchUserPhanTrang(res, page, pageSize, query) {
        try {
            let index = (page - 1) * pageSize;
            let checkName = await this.prisma.nguoiDung.findMany({ where: { ho_ten: { contains: query } }, skip: index, take: Number(pageSize) });
            let checkEmail = await this.prisma.nguoiDung.findMany({ where: { email: { contains: query } }, skip: index,
                take: Number(pageSize) });
            if (checkEmail.length !== 0) {
                checkEmail;
                (0, response_1.successCode)(res, checkEmail, "get data success");
            }
            else if (checkName.length !== 0) {
                checkName;
                (0, response_1.successCode)(res, checkName, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async thongTinTaiKhoan(res) {
        try {
            let data = await this.prisma.nguoiDung.findMany();
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error");
        }
    }
    async layThongTinNguoiDung(res, userId) {
        try {
            let checkIdUser = await this.prisma.nguoiDung.findFirst({ where: { tai_khoan: userId } });
            if (checkIdUser) {
                let data = checkIdUser;
                (0, response_1.successCode)(res, data, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async themNguoiDung(res, user) {
        try {
            let checEmail = await this.prisma.nguoiDung.findFirst({ where: { email: user.email } });
            let checkSDT = await this.prisma.nguoiDung.findFirst({ where: { so_dt: user.soDt } });
            if (checEmail == null) {
                if (checkSDT == null) {
                    let newData = { ho_ten: user.hoTen,
                        email: user.email,
                        so_dt: user.soDt,
                        mat_khau: (0, bcryptjs_1.hashSync)(user.matKhau, 10),
                        loai_nguoi_dung: "customer"
                    };
                    await this.prisma.nguoiDung.create({ data: newData });
                    (0, response_1.successCode)(res, "", "Add User success");
                }
                else {
                    (0, response_1.failCode)(res, null, "numberPhone existed");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "email existed");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async capNhatThongTinNguoiDung(res, userId, user) {
        try {
            let checkEmail = await this.prisma.nguoiDung.findFirst({ where: { email: user.email } });
            let checkSDT = await this.prisma.nguoiDung.findFirst({ where: { so_dt: user.soDt } });
            let checkUserById = await this.prisma.nguoiDung.findFirst({ where: { tai_khoan: userId } });
            if (checkUserById) {
                if (checkEmail == null || checkEmail.email == checkUserById.email) {
                    if (checkSDT == null || checkSDT.email == checkUserById.so_dt) {
                        let newData = { ho_ten: user.hoTen,
                            email: user.email,
                            so_dt: user.soDt,
                            mat_khau: (0, bcryptjs_1.hashSync)(user.matKhau, 10),
                            loai_nguoi_dung: "Customer"
                        };
                        await this.prisma.nguoiDung.update({ where: { tai_khoan: userId }, data: newData });
                        (0, response_1.successCode)(res, "", "Update user success");
                    }
                    else {
                        (0, response_1.failCode)(res, null, "numberPhone existed");
                    }
                }
                else {
                    (0, response_1.failCode)(res, null, "email existed");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async capNhatThongTinNguoiDungAdmin(res, user, userId) {
        try {
            let checkEmail = await this.prisma.nguoiDung.findFirst({ where: { email: user.email } });
            let checkSDT = await this.prisma.nguoiDung.findFirst({ where: { so_dt: user.soDt } });
            let checkUserById = await this.prisma.nguoiDung.findFirst({ where: { tai_khoan: userId } });
            if (checkUserById) {
                if (checkEmail == null || checkEmail.email == checkUserById.email) {
                    if (checkSDT == null || checkSDT.so_dt == checkUserById.so_dt) {
                        let newData = { ho_ten: user.hoTen,
                            email: user.email,
                            so_dt: user.soDt,
                            mat_khau: (0, bcryptjs_1.hashSync)(user.matKhau, 10),
                            loai_nguoi_dung: user.loaiNguoiDung
                        };
                        await this.prisma.nguoiDung.update({ where: { tai_khoan: userId }, data: newData });
                        (0, response_1.successCode)(res, "", "Update user success");
                    }
                    else {
                        (0, response_1.failCode)(res, null, "numberPhone existed");
                    }
                }
                else {
                    (0, response_1.failCode)(res, null, "email existed");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async xoaNguoiDung(res, userId) {
        try {
            let checkUserId = await this.prisma.nguoiDung.findFirst({ where: { tai_khoan: userId } });
            let checkIdDatve = await this.prisma.datVe.findFirst({ where: { tai_khoan: userId } });
            if (checkUserId) {
                if (checkIdDatve == null) {
                    await this.prisma.nguoiDung.delete({ where: { tai_khoan: userId } });
                    (0, response_1.successCode)(res, "", "Delete data success");
                }
                else {
                    (0, response_1.failCode)(res, null, "Can't delete user oredered ticket");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch (e) {
            (0, response_1.errorCode)(res, e.message);
        }
    }
    async getListLoaiND(res) {
        try {
            let data = await this.prisma.$queryRaw(client_1.Prisma.sql `select loai_nguoi_dung  from nguoiDung group by loai_nguoi_dung`);
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
};
exports.QuayLyNguoiDungService = QuayLyNguoiDungService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        config_1.ConfigService])
], QuayLyNguoiDungService);
//# sourceMappingURL=quay-ly-nguoi-dung.service.js.map