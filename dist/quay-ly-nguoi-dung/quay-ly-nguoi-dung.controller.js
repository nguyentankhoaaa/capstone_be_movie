"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuayLyNguoiDungController = void 0;
const common_1 = require("@nestjs/common");
const quay_ly_nguoi_dung_service_1 = require("./quay-ly-nguoi-dung.service");
const core_1 = require("@nestjs/common/decorators/core");
const passport_1 = require("@nestjs/passport");
let QuayLyNguoiDungController = exports.QuayLyNguoiDungController = class QuayLyNguoiDungController {
    constructor(quayLyNguoiDungService) {
        this.quayLyNguoiDungService = quayLyNguoiDungService;
    }
    getListLoaiND(res) {
        return this.quayLyNguoiDungService.getListLoaiND(res);
    }
    dangKy(body, res) {
        return this.quayLyNguoiDungService.dangKy(body, res);
    }
    dangNhap(body, res) {
        return this.quayLyNguoiDungService.dangNhap(body, res);
    }
    getListND(res) {
        return this.quayLyNguoiDungService.getListLoaiND(res);
    }
    getUserPhanTrang(res, page, pageSize) {
        return this.quayLyNguoiDungService.getUserPhanTrang(page, pageSize, res);
    }
    SearchNguoiDung(res, query) {
        return this.quayLyNguoiDungService.searchNguoiDung(res, query);
    }
    SearchUserPhanTrang(res, page, pageSize, query) {
        return this.quayLyNguoiDungService.searchUserPhanTrang(res, page, pageSize, query);
    }
    ThongTinTaiKhoan(res) {
        return this.quayLyNguoiDungService.thongTinTaiKhoan(res);
    }
    LayThongTinNguoiDung(res, userId) {
        return this.quayLyNguoiDungService.layThongTinNguoiDung(res, Number(userId));
    }
    ThemNguoiDung(res, body) {
        return this.quayLyNguoiDungService.themNguoiDung(res, body);
    }
    CapNhatThongTinNguoiDung(res, userId, body) {
        return this.quayLyNguoiDungService.capNhatThongTinNguoiDung(res, Number(userId), body);
    }
    CapNhatThongTinNguoiDungAd(res, body, userId) {
        return this.quayLyNguoiDungService.capNhatThongTinNguoiDungAdmin(res, body, Number(userId));
    }
    XoaNGuoiDung(res, userId) {
        return this.quayLyNguoiDungService.xoaNguoiDung(res, Number(userId));
    }
};
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachLoaiNguoiDung"),
    __param(0, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "getListLoaiND", null);
__decorate([
    (0, common_1.Post)("/DangKy"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "dangKy", null);
__decorate([
    (0, common_1.Post)("/dangNhap"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "dangNhap", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachNguoiDung"),
    __param(0, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "getListND", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayDanhSachNguoiDungPhanTrang/:page/:page_size"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("page")),
    __param(2, (0, common_1.Param)("page_size")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "getUserPhanTrang", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/TimKiemNguoiDung/:query"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("query")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "SearchNguoiDung", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/TimKiemNguoiDungPhanTrang/:page/:page_size/:query"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("page")),
    __param(2, (0, common_1.Param)("page_size")),
    __param(3, (0, common_1.Param)("query")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "SearchUserPhanTrang", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/ThongTinTaiKhoan"),
    __param(0, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "ThongTinTaiKhoan", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Get)("/LayThongTinNguoiDung/:user_id"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("user_id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "LayThongTinNguoiDung", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Post)("/ThemNguoiDung"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "ThemNguoiDung", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Put)("/CapNhatThongTinNGuoiDung/:user_id"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("user_id")),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "CapNhatThongTinNguoiDung", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Post)("/CapNhatThongTinNguoiDung/Admin/:user_id"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.Param)("user_id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "CapNhatThongTinNguoiDungAd", null);
__decorate([
    (0, core_1.UseGuards)((0, passport_1.AuthGuard)("jwt")),
    (0, common_1.Delete)("/XoaNguoiDung/:user_id"),
    __param(0, (0, common_1.Response)()),
    __param(1, (0, common_1.Param)("user_id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuayLyNguoiDungController.prototype, "XoaNGuoiDung", null);
exports.QuayLyNguoiDungController = QuayLyNguoiDungController = __decorate([
    (0, common_1.Controller)('api/quanLyNguoiDung'),
    __metadata("design:paramtypes", [quay_ly_nguoi_dung_service_1.QuayLyNguoiDungService])
], QuayLyNguoiDungController);
//# sourceMappingURL=quay-ly-nguoi-dung.controller.js.map