"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyDatVeModule = void 0;
const common_1 = require("@nestjs/common");
const quan_ly_dat_ve_service_1 = require("./quan-ly-dat-ve.service");
const quan_ly_dat_ve_controller_1 = require("./quan-ly-dat-ve.controller");
const jwt_1 = require("@nestjs/jwt");
const strategy_jwt_1 = require("../strategy/strategy.jwt");
let QuanLyDatVeModule = exports.QuanLyDatVeModule = class QuanLyDatVeModule {
};
exports.QuanLyDatVeModule = QuanLyDatVeModule = __decorate([
    (0, common_1.Module)({
        imports: [jwt_1.JwtModule.register({})],
        controllers: [quan_ly_dat_ve_controller_1.QuanLyDatVeController,],
        providers: [quan_ly_dat_ve_service_1.QuanLyDatVeService, strategy_jwt_1.JwtStrategy]
    })
], QuanLyDatVeModule);
//# sourceMappingURL=quan-ly-dat-ve.module.js.map