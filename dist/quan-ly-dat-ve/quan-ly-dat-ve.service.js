"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyDatVeService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const response_1 = require("../config/response");
let QuanLyDatVeService = exports.QuanLyDatVeService = class QuanLyDatVeService {
    constructor() {
        this.prisma = new client_1.PrismaClient();
    }
    async datVe(userId, maLichChieu, maGhe, res) {
        try {
            let checkUserbyId = await this.prisma.nguoiDung.findFirst({ where: { tai_khoan: userId } });
            let checkListLichChieu = await this.prisma.datVe.findMany({ where: { ma_lich_chieu: maLichChieu } });
            let newData = { tai_khoan: userId, ma_lich_chieu: maLichChieu, ma_ghe: maGhe };
            let checkLichChieu = await this.prisma.datVe.findFirst({ where: { ma_lich_chieu: maLichChieu } });
            let checkDatVe = await this.prisma.datVe.findMany();
            let giaVe = await this.prisma.lichChieu.findFirst({ where: { ma_lich_chieu: maLichChieu } });
            let dataShow = { maLichChieu, danhSachVe: { maGhe, giaVe: giaVe.gia_ve } };
            if (checkUserbyId) {
                if (checkLichChieu != null) {
                    for (const lichChieu of checkListLichChieu) {
                        if (lichChieu.ma_ghe == maGhe) {
                            (0, response_1.failCode)(res, null, "ticket ordered");
                        }
                    }
                    let index = checkDatVe.findIndex(item => item.ma_ghe == maGhe && item.ma_lich_chieu == maLichChieu);
                    if (index == -1) {
                        await this.prisma.datVe.create({ data: newData });
                        (0, response_1.successCode)(res, dataShow, "order success");
                    }
                    else {
                        (0, response_1.failCode)(res, null, "ticket ordered");
                    }
                }
                else {
                    await this.prisma.datVe.create({ data: newData });
                    (0, response_1.successCode)(res, dataShow, "order success");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "user not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    ;
    async getListPhongVe(maLichChieu, res) {
        try {
            let data = await this.prisma.lichChieu.findFirst({ include: { rapPhim: true }, where: { ma_lich_chieu: maLichChieu } });
            (0, response_1.successCode)(res, data, "get data success");
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async taoLichChieu(lichChieu, res) {
        try {
            let checkIdPhim = await this.prisma.phim.findFirst({ where: { ma_phim: lichChieu.maPhim } });
            let checkIdRap = await this.prisma.rapPhim.findFirst({ where: { ma_rap: lichChieu.maRap } });
            let checkListLichChieu = await this.prisma.lichChieu.findMany();
            let indexLichChieu = checkListLichChieu.findIndex(item => item.ma_phim == lichChieu.maPhim
                && item.ma_rap == lichChieu.maRap
                && item.ngay_gio_chieu.getTime() == new Date(lichChieu.ngayGioChieu).getTime());
            if (checkIdRap) {
                if (checkIdPhim) {
                    if (indexLichChieu == -1) {
                        let newData = { ma_phim: lichChieu.maPhim,
                            ma_rap: lichChieu.maRap,
                            ngay_gio_chieu: new Date(lichChieu.ngayGioChieu),
                            gia_ve: lichChieu.giaVe
                        };
                        let data = await this.prisma.lichChieu.create({ data: newData });
                        (0, response_1.successCode)(res, data, "create data success");
                    }
                    else {
                        (0, response_1.failCode)(res, null, "lich Chieu existed");
                    }
                }
                else {
                    (0, response_1.failCode)(res, null, "id movie not exist");
                }
            }
            else {
                (0, response_1.failCode)(res, null, "id center movie not exist");
            }
        }
        catch (error) {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
};
exports.QuanLyDatVeService = QuanLyDatVeService = __decorate([
    (0, common_1.Injectable)()
], QuanLyDatVeService);
//# sourceMappingURL=quan-ly-dat-ve.service.js.map