import { PrismaClient } from '@prisma/client';
export declare class QuanLyDatVeService {
    prisma: PrismaClient<import(".prisma/client").Prisma.PrismaClientOptions, never, import("@prisma/client/runtime/library").DefaultArgs>;
    datVe(userId: any, maLichChieu: any, maGhe: any, res: any): Promise<void>;
    getListPhongVe(maLichChieu: any, res: any): Promise<void>;
    taoLichChieu(lichChieu: any, res: any): Promise<void>;
}
