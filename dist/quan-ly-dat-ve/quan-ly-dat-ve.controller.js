"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyDatVeController = void 0;
const common_1 = require("@nestjs/common");
const quan_ly_dat_ve_service_1 = require("./quan-ly-dat-ve.service");
const decorators_1 = require("@nestjs/common/decorators");
const dist_1 = require("@nestjs/passport/dist");
let QuanLyDatVeController = exports.QuanLyDatVeController = class QuanLyDatVeController {
    constructor(quanLyDatVeService) {
        this.quanLyDatVeService = quanLyDatVeService;
    }
    datVe(userId, res, maLichChieu, maGhe) {
        return this.quanLyDatVeService.datVe(Number(userId), Number(maLichChieu), Number(maGhe), res);
    }
    getListPhongVe(maLichChieu, res) {
        return this.quanLyDatVeService.getListPhongVe(Number(maLichChieu), res);
    }
    taoLichChieu(body, res) {
        return this.quanLyDatVeService.taoLichChieu(body, res);
    }
};
__decorate([
    (0, common_1.Post)("/datVe/:tai_khoan/:ma_lich_chieu/:ma_ghe"),
    __param(0, (0, common_1.Param)("tai_khoan")),
    __param(1, (0, common_1.Response)()),
    __param(2, (0, common_1.Param)("ma_lich_chieu")),
    __param(3, (0, common_1.Param)("ma_ghe")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyDatVeController.prototype, "datVe", null);
__decorate([
    (0, common_1.Get)("/LayDanhSachPhongVe/:ma_lich_chieu"),
    __param(0, (0, common_1.Param)("ma_lich_chieu")),
    __param(1, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyDatVeController.prototype, "getListPhongVe", null);
__decorate([
    (0, common_1.Post)("/TaoLichChieu"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Response)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuanLyDatVeController.prototype, "taoLichChieu", null);
exports.QuanLyDatVeController = QuanLyDatVeController = __decorate([
    (0, decorators_1.UseGuards)((0, dist_1.AuthGuard)("jwt")),
    (0, common_1.Controller)('api/quanLyDatve'),
    __metadata("design:paramtypes", [quan_ly_dat_ve_service_1.QuanLyDatVeService])
], QuanLyDatVeController);
//# sourceMappingURL=quan-ly-dat-ve.controller.js.map