import { QuanLyDatVeService } from './quan-ly-dat-ve.service';
export declare class QuanLyDatVeController {
    private readonly quanLyDatVeService;
    constructor(quanLyDatVeService: QuanLyDatVeService);
    datVe(userId: any, res: any, maLichChieu: any, maGhe: any): Promise<void>;
    getListPhongVe(maLichChieu: any, res: any): Promise<void>;
    taoLichChieu(body: any, res: any): Promise<void>;
}
