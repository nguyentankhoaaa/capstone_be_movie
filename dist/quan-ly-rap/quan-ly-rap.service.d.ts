import { PrismaClient } from '@prisma/client';
export declare class QuanLyRapService {
    prisma: PrismaClient<import(".prisma/client").Prisma.PrismaClientOptions, never, import("@prisma/client/runtime/library").DefaultArgs>;
    layThongTinHeThongRap(res: any, maHTR: any): Promise<void>;
    layThongTinCumRapTheoHeThong(res: any, maHTR: any): Promise<void>;
    layThongTinLichChieuHeThongRap(res: any, maHTR: any): Promise<void>;
    layThongTinLichChieuPhim(res: any, maPhim: any): Promise<void>;
}
