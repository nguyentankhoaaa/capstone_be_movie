import { QuanLyRapService } from './quan-ly-rap.service';
export declare class QuanLyRapController {
    private readonly quanLyRapService;
    constructor(quanLyRapService: QuanLyRapService);
    LayThongTinHeThongRap(res: any, maHTR: any): Promise<void>;
    LayThongTinCumRapTheoHeThong(res: any, maHTR: any): Promise<void>;
    LayThongTinLichChieuHeThongRap(res: any, maHTR: any): Promise<void>;
    LayThongTinLichChieuPhim(res: any, maPhim: any): Promise<void>;
}
