"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.QuanLyRapService = void 0;
const common_1 = require("@nestjs/common");
const client_1 = require("@prisma/client");
const response_1 = require("../config/response");
let QuanLyRapService = exports.QuanLyRapService = class QuanLyRapService {
    constructor() {
        this.prisma = new client_1.PrismaClient();
    }
    async layThongTinHeThongRap(res, maHTR) {
        try {
            let checkmaHTR = await this.prisma.heThongRap.findFirst({ where: { ma_he_thong_rap: maHTR } });
            if (checkmaHTR) {
                let data = checkmaHTR;
                (0, response_1.successCode)(res, data, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "he thong rap not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layThongTinCumRapTheoHeThong(res, maHTR) {
        try {
            let checkmaHTR = await this.prisma.heThongRap.findFirst({ where: { ma_he_thong_rap: maHTR } });
            if (checkmaHTR) {
                let data = await this.prisma.heThongRap.findFirst({ include: { cumRap: true }, where: { ma_he_thong_rap: maHTR } });
                (0, response_1.successCode)(res, data, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "he thong rap not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layThongTinLichChieuHeThongRap(res, maHTR) {
        try {
            let checkmaHTR = await this.prisma.heThongRap.findFirst({ where: { ma_he_thong_rap: maHTR } });
            if (checkmaHTR) {
                let data = await this.prisma.heThongRap.findFirst({ include: { cumRap: { include: { rapPhim: { include: { lichChieu: true } } } } }, where: { ma_he_thong_rap: maHTR } });
                (0, response_1.successCode)(res, data, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "he thong rap not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
    async layThongTinLichChieuPhim(res, maPhim) {
        try {
            let checkMaPhim = await this.prisma.phim.findFirst({ where: { ma_phim: maPhim } });
            if (checkMaPhim) {
                let data = await this.prisma.phim.findFirst({ include: { lichChieu: true }, where: { ma_phim: maPhim } });
                (0, response_1.successCode)(res, data, "get data success");
            }
            else {
                (0, response_1.failCode)(res, null, "movie not exist");
            }
        }
        catch {
            (0, response_1.errorCode)(res, "error BE");
        }
    }
};
exports.QuanLyRapService = QuanLyRapService = __decorate([
    (0, common_1.Injectable)()
], QuanLyRapService);
//# sourceMappingURL=quan-ly-rap.service.js.map